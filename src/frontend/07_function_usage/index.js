window.onload = () => {
    const output = document.getElementById('output');
    // self executing function
    (() => {
        console.log('Self executing function');
    })();

    // returned from a function call
    function returner() {
        return () => {
            console.log('Function returned from a function');
        };
    }
    returner()();

    // passed as parameter: callbacks
    function callbacker(callback) {
        setTimeout(callback, 3000);
    }
    callbacker(() => alert('Run after couple of seconds'));

    // Ajax sample using callbacks
    function get(uri, callback) {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = () => {
            if (httpRequest.readyState === XMLHttpRequest.DONE) {
                callback(httpRequest.responseText);
            }
        }
        httpRequest.open('GET', uri, true);
        httpRequest.send();
    }
    get('/data/users.json', (result) => {
        var users = JSON.parse(result);
        users.forEach((user) => {
            get(`/data/${user.id}.json`, function(result) {
                console.log(this.name, result);
            }.bind(user));
        });
    });
}