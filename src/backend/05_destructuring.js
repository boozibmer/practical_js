const { Content } = require('./utils');
let content = new Content('Destructuring');

var a, b, rest;
[a, b] = [10, 20];
content.echo('[a, b]', '[10, 20]');
content.echo('a', a); // 10
content.echo('b', b); // 20
content.br();

// [a, b, ...rest] = [10, 20, 30, 40, 50];
// content.echo('[a, b, ...rest]', '[10, 20, 30, 40, 50]');
// content.echo('a', a); // 10
// content.echo('b', b); // 20
// content.echo('rest', rest); // [30, 40, 50]
// content.br();

// ({ a, b } = { a: 10, b: 20 });
// content.echo('{ a, b }', '{ a: 10, b: 20 }');
// content.echo('a', a); // 10
// content.echo('b', b); // 20
// content.br();


// Stage 4(finished) proposal
// ({a, b, ...rest} = {a: 10, b: 20, c: 30, d: 40});
// content.echo('{a, b, ...rest}', '{a: 10, b: 20, c: 30, d: 40}');
// content.echo('a', a); // 10
// content.echo('b', b); // 20
// content.echo('rest', rest); // {c: 30, d: 40}

module.exports = { output: content.render() };