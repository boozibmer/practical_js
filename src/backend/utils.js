const backLink = '<a href="/"><span class="back-btn">BACK</span></a>';
const jsMeaning = '<div id="js-meaning">is a high-level, interpreted programming language that conforms to the ECMAScript specification. JavaScript has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.</div>';
const render = (title, content) => (`<h1>${title}</h1><p>${content}</p><p class="buttons">${backLink}</p>`);
const span = (cls, val) => (`<span class="${cls}">${val}</span>`);
const h2 = (val) => (`<p class="sub-title">${val}</p>`);
const echo = (labelTxt, valueTxt) => {
    const label = labelTxt ? span('label', labelTxt + ':') : '';
    const value = valueTxt ? span('value', valueTxt) : '';
    return `${label}${value}<br />`;
}
class Content {
    constructor(title) {
        this.title = title;
        this.content = '';
        this.add = (val) => (this.content += val);
        this.h2 = (val) => this.add(h2(val));
        this.span = (cls, val) => this.add(span(cls, val));
        this.echo = (label, text) => this.add(echo(label, text));
        this.asString = () => {
            return this.content;
        }
    }

    br() {
        this.add('<br />');
    }

    render() {
        return render(this.title, this.content);
    }
}

module.exports = {
    backLink,
    jsMeaning,
    render,
    echo,
    h2,
    Content,
};