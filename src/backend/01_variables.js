const { Content } = require('./utils');
let content = new Content('Variables');

// declaring variables
var testVar = 1;// global scope
let testLet = 2;// has local block scope
const testConst = 3;// needs to be initialized or will throw an error

// if (true) {
//     var testVar = 10;
//     let testLet = 20;
//     const testConst = 30;
//     content.h2('IF Scope');
//     content.echo('testVar', testVar);
//     content.echo('testLet', testLet);
//     content.echo('testConst', testConst);
// }

content.h2('Global Scope');
content.echo('testVar', testVar);
content.echo('testLet', testLet);
content.echo('testConst', testConst);

module.exports = { output: content.render() };