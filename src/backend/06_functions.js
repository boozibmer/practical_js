const { Content } = require('./utils');
let content = new Content('Functions');

// ways to declare a function
// 1. Function declaration
function declared(param) {
    content.echo('Function type', param);
    return true;
}
declared('declared');
content.br();

// 2. Function expression
const expressed = function(param) {
    content.echo('Function type', param);
    return true;
};
expressed('expressed');
content.br();

// 3. Shorthand method: can be used in a method declaration on object literals and ES6 classes
const shortHand = {
    name: 'shortHand',
    sayHi(friend) {
        content.add(`Hi ${friend}! I'm a ${this.name} function<br />`);
    }
}
shortHand.sayHi('Guys');
content.br();

// 3.1. Computed shorthand method
var methodName = 'say' + 'Hi';
const shortHandComputed = {
    name: 'shortHandComputed',
    [methodName](friend) {
        content.add(`Hi ${friend}! I'm a ${this.name} function<br />`);
    }
}
shortHandComputed.name = 'Computed';
shortHandComputed[methodName]('Guys');
content.br();

// 4. Arrow function, if you don't need to use this, use arrow functions
const arrow1 = (param) => {
    content.echo('From arrow 1', param);
}
arrow1('test param');
const arrow2 = param => (content.echo('From arrow 2', param));
arrow2('test param');

const arrowReturn = (param) => (`Returned value : ${param}`);
content.echo('Arrow function returning', arrowReturn('test param'));
content.br();

class Arrow {
    constructor() {
        this.property = 'The value of property';
    }
    oldSchool() {
        return function() {
            if (this) {
                content.echo('From olds school function', this.property);
            } else {
                content.echo('Olds school function this', this);
            }
        };
    }
    arrowFunc() {
        return () => {
            content.echo('From arrow function', this.property);
        };
    }
}
let ar = new Arrow();
ar.oldSchool()();
ar.arrowFunc()();


module.exports = { output: content.render() };