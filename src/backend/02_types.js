const { Content } = require('./utils');
let content = new Content('Types');

let num = 99.99;
// let str = 'This ' + "is " + `a number: ${num}`;
// let boolean = true;
// let theFunction = function() { alert('Hi'); };
// let theUndefined;
// let theNull = null;
// let obj = { val: 'a value' };

// content.echo('num', typeof num);
// content.echo('str', typeof str);
// content.echo('boolean', typeof boolean);
// content.echo('theFunction', typeof theFunction);
// content.echo('theUndefined', typeof theUndefined);
// content.echo('theNull', typeof theNull);
// content.echo('obj', typeof obj);

module.exports = { output: content.render() };