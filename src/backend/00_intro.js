const { Content, jsMeaning } = require('./utils');
let content = new Content('Introduction');

content.add(jsMeaning);

content.h2('Before ES6:');
content.echo(null, 'Everything javascript before ES6');
content.echo(null, 'events, prototype based oop, dom');
content.br();

content.h2('6th Edition or ES6 or ECMAScript 2015');
content.echo(null, 'class declarations (class Foo { ... })');
content.echo(null, 'ES6 modules like import * as moduleName from "..."; export const Foo');
content.echo(null, 'arrow function expression (() => {...})');
content.echo(null, 'let keyword for local declarations');
content.echo(null, 'const keyword for constant variable declarations');
content.echo(null, 'new collections (maps, sets and WeakMap)');
content.echo(null, 'promises');
content.echo(null, 'template strings');

content.h2('7th Edition - ECMAScript 2016');
content.echo(null, 'variable destructuring');
content.echo(null, 'exponentiation operator **');
content.echo(null, 'await, async keywords for asynchronous programming');

content.h2('Server Side Javascript: NodeJS');

module.exports = { output: content.render() };