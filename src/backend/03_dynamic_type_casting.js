const { Content } = require('./utils');
let content = new Content('Dynamic Typing and Casting');

// DYNAMIC TYPING
let dynamicVar = 'String content';
// assigned a new value w/ new type
// dynamicVar = 333;
// assigned a new value w/ new type
// dynamicVar = [1, 2, 3];
content.echo('dynamicVar', dynamicVar);




// CASTING
let str = '10.1';
// not calculated
let num = str + 10;

// cast to number for calculations
// let num = Number(str) + 10;

// content.echo('num', num);

module.exports = { output: content.render() };