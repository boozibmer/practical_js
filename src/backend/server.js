const express = require('express');
const fs = require('fs');
const Mustache = require('mustache');
const utils = require('./utils');
const app = express();
const port = 3000;

Mustache.escape = function(text) {return text;};

const lectures = [
    '00_intro',
    '01_variables',
    '02_types',
    '03_dynamic_type_casting',
    '04_collections',
    '05_destructuring',
    '06_functions',
];

const frontend = [
    '07_function_usage'
];

const renderHTML = (name, template) => {
    const { output } = require(`./${name}`);
    return Mustache.render(template, { output });
}

fs. readFile('./src/backend/template.html', 'utf8', (err, template) => {
    var home = `<h1>Javascript Brownbag</h1>`;
    home += '<ol>';
    [...lectures, ...frontend].forEach(name => (home += `<li><a href="/${name}">${name}</a></li>`));
    home += '</ol>';
    app.get(`/`, (req, res) => res.send(Mustache.render(template, { output: home })));
    lectures.forEach(name => app.get(`/${name}`, (req, res) => res.send(renderHTML(name, template))));
    app.use(express.static('src/frontend'));
    app.listen(port, () => console.log(`App started on port ${port}!`));
});