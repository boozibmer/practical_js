const { Content } = require('./utils');
let content = new Content('Collections');

// ARRAY
let theArray = [1, 2, 'string', { val: 'val' }, null];
content.echo('theArray type', typeof theArray);
content.echo('theArray contents', theArray);

content.br();

// SET
// let theSet = new Set([1, 2, 2, 3, 3, 3, 4, 4, 4]);
// content.echo('theSet type', typeof theSet);
// let setVals = '';
// for (let o of theSet) {
//     setVals += o + ', ';
// }
// content.echo('theSet contents', Array.from(theSet));

content.br();

// MAP
// let theMap = new Map();
// theMap.set('key1', 'value1');
// theMap.set('key2', 'value2');
// content.echo('theMap type', typeof theMap);
// let mapVals = '';
// for (var [key, value] of theMap) {
//     mapVals += key + ': ' + value + ', ';
// }
// content.echo('theMap contents', mapVals);

module.exports = { output: content.render() };